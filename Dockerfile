FROM python:3.7-alpine as builder

ENV PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    PIP_NO_CACHE_DIR=on \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100

RUN apk update \
    && apk add --no-cache \
    jpeg-dev \
    zlib-dev \
    gcc \
    musl-dev \
    postgresql-dev \
    libffi-dev \
    && pip install poetry

WORKDIR /app

COPY poetry.lock .
COPY pyproject.toml .

RUN poetry config virtualenvs.in-project true \
    && poetry install --no-root --no-dev --no-interaction

FROM python:3.7-alpine

WORKDIR /app

COPY --from=builder /app/.venv /app/.venv
COPY . .

RUN apk update && apk add --no-cache postgresql-dev jpeg-dev

VOLUME [ "/app/intake_platform/static" ]

ENV PATH="/app/.venv/bin:$PATH"
ENTRYPOINT [ "./run.sh" ]
