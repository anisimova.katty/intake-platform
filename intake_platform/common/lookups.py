from selectable.base import ModelLookup
from selectable.registry import registry

from .models import Location


class LocationLookup(ModelLookup):
    model = Location
    search_fields = ("city__icontains",)


registry.register(LocationLookup)
