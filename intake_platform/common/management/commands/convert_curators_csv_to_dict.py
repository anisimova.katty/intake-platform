import pprint
from collections import defaultdict

from django.core.management.base import BaseCommand

# from common.models import UserProfile
from common.utils import UnicodeReader


class Command(BaseCommand):
    help = u"Converts curators csv list to curators dict in expected format"

    def add_arguments(self, parser):
        parser.add_argument("filename", help="filepath to read", type=str)

    def handle(self, *args, **options):
        pp = pprint.PrettyPrinter(indent=2)
        d = {}
        with open(options["filename"], "r") as fp:
            reader = UnicodeReader(fp)
            for row in reader:
                w_slug = row[0]
                # login = row[1]
                email = row[2]
                # password = row[3]

                d[email] = {"workshop_slug": w_slug}

        pp.pprint(d)
