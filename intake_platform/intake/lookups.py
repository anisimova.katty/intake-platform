from selectable.base import ModelLookup
from selectable.registry import registry
from selectable.decorators import login_required

from .models import ApplicationCase, WorkshopPetitionConnection
from intake_platform.common.tools import get_user
from django.db.models import Q


@login_required
class ApplicationCaseLookup(ModelLookup):
    model = ApplicationCase
    search_fields = (
        "user__email__icontains",
        "user__first_name__icontains",
        "user__last_name__icontains",
    )

    def get_item_value(self, item):
        return self.get_item_label(item)

    def get_item_label(self, item):
        first_name = item.user.first_name
        last_name = item.user.last_name
        email = item.user.email
        return u"%s %s (%s)" % (first_name, last_name, email)


registry.register(ApplicationCaseLookup)


@login_required
class CuratorApplicationCaseLookup(ModelLookup):
    model = ApplicationCase
    search_fields = (
        "user__email__icontains",
        "user__first_name__icontains",
        "user__last_name__icontains",
    )

    def get_query(self, request, term):
        q = Q(user__email__icontains=term)
        q |= Q(user__last_name__icontains=term)
        q |= Q(user__first_name__icontains=term)
        users = []
        for wp in request.user.workshopprogrammetainfo_set.all():
            users += [
                p.user
                for p in WorkshopPetitionConnection.objects.filter(
                    workshop_slug=wp.workshop_slug, program_slug=wp.program_slug
                )
            ]
        qs = ApplicationCase.objects.filter(user__in=users, is_closed=True).filter(q)
        return qs

    def get_item_value(self, item):
        return self.get_item_label(item)

    def get_item_label(self, item):
        first_name = item.user.first_name
        last_name = item.user.last_name
        email = item.user.email
        return u"%s %s (%s)" % (first_name, last_name, email)


registry.register(CuratorApplicationCaseLookup)
