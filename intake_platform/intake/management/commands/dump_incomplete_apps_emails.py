# coding: utf-8

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

from intake.models import WorkshopProgramMetaInfo
from common.models import UserProfile
from intake.tools import get_wp_accepted_apps, has_buck


class Command(BaseCommand):
    help = "Dumps all emails of incomplete apps"

    def handle(self, *args, **options):
        users_all = list(User.objects.all())
        non_idiots = [x for x in users_all if has_buck(x, "passed_idiot_test")]
        complete_1st = [x for x in users_all if has_buck(x, "complete_first_step")]

        for u in [x for x in non_idiots if x not in complete_1st]:
            print(u.email)
