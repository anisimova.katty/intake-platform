# coding: utf-8

from django.core.management.base import BaseCommand
from django.core import serializers

from django.contrib.auth.models import User
import intake.models as mm

# USERS_ID_RANGE = xrange(200, 300)
OUTPUT = "data_rvc2017"

DB_USING = "data_2017"


class Command(BaseCommand):
    help = """Dump intake Data for Users with PKs in range %s.
Output is written to separate files in folder %s""" % (
        200,
        OUTPUT,
    )

    def handle(self, *args, **options):
        users_ids = set(
            [
                x.user.pk
                for x in mm.UserBuck.objects.using(DB_USING).filter(
                    slug="accepted_by_workshop"
                )
            ]
        )

        def foo(m):
            return m.objects.using(DB_USING).filter(user__pk__in=users_ids)

        def baz(x):
            return foo(x).count()

        users = User.objects.using(DB_USING).filter(pk__in=users_ids)
        print("I haz %s users accepted" % users.count())

        with open("%s/wmetas.JSON" % OUTPUT, "w") as fp:
            metas = mm.WorkshopProgramMetaInfo.objects.using(DB_USING).all()
            data = serializers.serialize("json", metas)
            fp.write(data)

        for fname, model in [
            ("workpetcon", mm.WorkshopPetitionConnection),
            ("addit_cont", mm.UserAdditionalContacts),
            ("educ_info", mm.UserEducationInfo),
            ("act_info", mm.UserActivityInfo),
            # ('handyman', mm.UserHandymanshipInfo),
            ("gibeba", mm.GiveBackInfo),
            ("pastfut", mm.PastFuturePresentInfo),
            ("logistics", mm.UserLogisticsInfo),
            ("under_ui", mm.UnderageUserInfo),
        ]:
            fname = "%s/%s.json" % (OUTPUT, fname)
            with open(fname, "w") as fp:
                data = serializers.serialize("json", foo(model))
                fp.write(data)
                print("%s has %s entries" % (fname, baz(model)))
