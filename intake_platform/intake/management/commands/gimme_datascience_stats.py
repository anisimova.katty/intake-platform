# coding: utf-8

from django.core.management.base import BaseCommand
import openpyxl
import datetime
from openpyxl.cell import get_column_letter

from intake.tools import get_wp_accepted_apps
from intake.tools import get_wp_passed_apps


class Command(BaseCommand):
    help = "Show various info and stats"

    def handle(self, *args, **options):
        # Find users with buck passed

        # w_slug, p_slug = 'datascience', 'main'
        w_slug, p_slug = "deeplearning", "main"
        now_str = datetime.datetime.now().strftime("%Y%b%d").lower()
        fname = "%s-%s_info_%s.xlsx" % (w_slug, p_slug, now_str)

        workbook = openpyxl.Workbook()  # outfile, {'in_memory': True})
        worksheet = workbook.get_active_sheet()

        for col_num in [1, 2, 3, 4, 6, 7, 8, 9]:
            c = worksheet.cell(row=1, column=col_num)
            c.style.font.bold = True
            column_letter = get_column_letter(col_num)
            worksheet.column_dimensions[column_letter].width = 60

        col_num = 5  # Location column
        c = worksheet.cell(row=1, column=col_num)
        c.style.font.bold = True
        column_letter = get_column_letter(col_num)
        worksheet.column_dimensions[column_letter].width = 80

        col_num = 6  # Email?
        c = worksheet.cell(row=1, column=col_num)
        c.style.font.bold = True
        column_letter = get_column_letter(col_num)
        worksheet.column_dimensions[column_letter].width = 100

        # col_num = 3 # Education column
        # c = worksheet.cell(row=1, column=col_num)
        # c.style.font.bold = True
        # column_letter = get_column_letter(col_num)
        # worksheet.column_dimensions[column_letter].width = 120

        def foo(a):
            for k, v in a.full_info().items():
                if k in ["first_name", "last_name", "email", "location", "birthdate"]:
                    yield v
                elif k == "uei":
                    yield v.get_finished_ed_display()
                    yield v.affiliation
                elif k == "uac":
                    yield v.vk
                    yield v.fb
                elif k == "uact":
                    yield v.main_occupation
                    yield v.hobbies

        for col_num in range(len(row)):
            c = worksheet.cell(row=i, column=col_num + 1)
            c.value = row[col_num]
            c.style.alignment.wrap_text = True
            print(("    %s" % row[col_num]))

        # for i, a in enumerate(get_wp_accepted_apps(w_slug, p_slug), start=1):
        for i, a in enumerate(get_wp_passed_apps(w_slug, p_slug), start=2):
            row = [x for x in foo(a)]
            print(a)
            print((len(row)))
            for col_num in range(len(row)):
                c = worksheet.cell(row=i, column=col_num + 1)
                c.value = row[col_num]
                c.style.alignment.wrap_text = True
                print(("    %s" % row[col_num]))

                # for j, f in enumerate([f for (_, f) in app.csv_fields()]):
                # worksheet.write(i, j, u"%s" % f)

        workbook.save(fname)
        return fname
