# coding: utf-8

from datetime import datetime

from django.core.management.base import BaseCommand

from intake_platform.intake.models import ApplicationCase, UserBuck
from intake_platform.common.models import UserProfile
from intake_platform.intake.tools import has_buck


class Command(BaseCommand):
    help = "Opens user cases en masse"

    def handle(self, *args, **options):
        qs = UserProfile.objects.filter(is_nabor=False)
        data = []
        for u in qs:
            if has_buck(u.user, "complete_first_step"):
                data += [
                    (u, UserBuck.objects.get(user=u.user, slug="complete_first_step"))
                ]

        counter = 0
        for u, buck in data:
            case, created = ApplicationCase.objects.get_or_create(user=u.user)

            if created:
                case.created = buck.when
                case.last_modified = datetime.now()
                case.save()
                print("created case for App with date %s" % buck.when)
                print("App_id: %s" % case.pk)

                counter += 1

        print("total cases created:", counter)
