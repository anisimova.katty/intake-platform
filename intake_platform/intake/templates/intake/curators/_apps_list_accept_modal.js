<script language="javascript">
$("#modal_acceptapp").on('show.bs.modal', function(event) {
  var button = $(event.relatedTarget);
  var a_pk = button.data('applicant');
  var a_workshop = button.data('workshop');
  var a_program = button.data('program');
  var recipient_name = button.data('fullname');

  var modal = $(this);

  modal.find('.modal-title').text('Подтверждаем приём анкеты #' + a_pk);
  modal.find('#a_pk').val(a_pk);
  modal.find('#a_workshop').text(a_workshop);
  modal.find('#a_program').text(a_program);
  modal.find('#recipient-name').text(recipient_name);

  modal.find('#back_url').val(window.location.pathname);
  modal.find('.modal-footer button#submit_btn').on('click', function(event) {
          modal.find('.modal-body form').submit();
  });
});
</script>
