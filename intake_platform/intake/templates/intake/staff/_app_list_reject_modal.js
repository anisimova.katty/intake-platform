<script language="javascript">
$("#modal_returnapp").on('show.bs.modal', function(event) {
  var button = $(event.relatedTarget);
  var a_pk = button.data('applicant');
  var recipient_name = button.data('fullname');

  var modal = $(this);

  modal.find('.modal-title').text('Причина возврата анкеты #' + a_pk);
  modal.find('#a_pk').val(a_pk);
  modal.find('#recipient-name').val(recipient_name);

  modal.find('#back_url').val(window.location.pathname);
  modal.find('.modal-footer button#submit_btn').on('click', function(event) {
          modal.find('.modal-body form').submit();
  });
});
</script>
