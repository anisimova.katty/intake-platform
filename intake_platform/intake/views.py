# coding: utf-8

import os
from datetime import datetime
import random

from annoying.decorators import ajax_request, render_to
from annoying.functions import get_config, get_object_or_None
from intake_platform.common.tools import get_user, get_workshops_info
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from wsgiref.util import FileWrapper
from django.urls import reverse
from django.dispatch import receiver
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.template import Context, loader
from django.utils import html
from django.utils.decorators import method_decorator
from django.views import generic
from intake_platform.intake_platform.forms import FancySignupForm, default_signup_fields

from .decorators import user_has_no_buck_with_slug
from .forms import (
    ActivityInfoForm,
    AdditionalContactsForm,
    AssignmentResponseForm,
    BasicInfoForm,
    EducationInfoForm,
    GiveBackInfoForm,
    HandymanshipInfoForm,
    LeakageInfoForm,
    LogisticsInfoForm,
    MoneyInfoForm,
    PastFuturePresentInfoForm,
    PhotoReplaceForm,
    PhotoUploadForm,
    UnderageUserInfoForm,
    UserNotDumbForm,
    WorkshopProgramSelectForm,
    WPCMotivationForm,
)
from .models import (
    AppEmail,
    ApplicationAssignmentResponse,
    ApplicationCase,
    GiveBackInfo,
    PastFuturePresentInfo,
    UnderageUserInfo,
    User,
    UserActivityInfo,
    UserAdditionalContacts,
    UserEducationInfo,
    UserHandymanshipInfo,
    UserLogisticsInfo,
    UserPhoto,
    WorkshopPetitionConnection,
)
from .tools import (
    attach_buck,
    create_locked_wpc,
    get_wp_slugs_and_names,
    has_buck,
    prepare_second_round,
    robot_comment_on_case,
    send_mail_to_applicant,
    user_1st_step_info,
)


def try_fetching_old_data(sender, **kwargs):
    # assuming that we have defined 'data_2015' as database
    last_year_db = "data_%s" % (datetime.today().year - 1)

    if last_year_db in settings.DATABASES:
        user = kwargs.pop("user")
        old_user = get_object_or_None(
            User.objects.using(last_year_db), email=user.email
        )
        if old_user is None:
            return

        old_data = user_1st_step_info(old_user, db_tag=last_year_db)

        related_cls = {
            # 'WorkshopPetitionConnection': WorkshopPetitionConnection,
            "UserAdditionalContacts": UserAdditionalContacts,
            # 'UserEducationInfo': UserEducationInfo,
            "UserActivityInfo": UserActivityInfo,
            # 'UserHandymanshipInfo': UserHandymanshipInfo,
            # 'GiveBackInfo': GiveBackInfo,
            # 'UserLogisticsInfo': UserLogisticsInfo,
            # 'PastFuturePresentInfo': PastFuturePresentInfo,
            "UserPhoto": UserPhoto,
        }

        for cls_tag, obj in old_data["tuples"]:
            if (obj is None) or (cls_tag not in related_cls):
                continue
            d = obj.to_dict()
            d["user"] = user
            c = related_cls[cls_tag](**d)
            c.save()


@login_required
@user_has_no_buck_with_slug("passed_idiot_test")
@render_to("intake/check_user_not_dumb_prequel.html")
def check_user_not_dumb_prequel(request):
    return {}


@login_required
@user_has_no_buck_with_slug("passed_idiot_test")
@render_to("intake/check_user_not_dumb_actual_test.html")
def check_user_not_dumb_actual_test(request):
    u = get_user(request)

    if request.method == "POST":
        f = UserNotDumbForm(request.POST)
        if f.is_valid():
            attach_buck(u, "passed_idiot_test")
            messages.success(request, 'Тест пройден, всё верно!')
            return redirect("profile")
        else:
            messages.error(
                request, 'В тесте были неправильные ответы. Попробуем ещё раз'
            )
    else:
        f = UserNotDumbForm()

    return {"form": f}


@login_required
@user_has_no_buck_with_slug("complete_first_step")
@render_to("intake/workshop_program_select.html")
def wp_select(request):
    u = get_user(request)

    if not u.userprofile.birthdate:
        messages.error(
            request,
            ' '.join(
                [
                    'Прежде чем выбирать мастерскую, нам надо знать ваш возраст',
                    '(заполните раздел "Базовая информация")',
                ]
            )
        )
        return redirect('profile')

    wpc = get_object_or_None(WorkshopPetitionConnection, user=u)
    if request.method == "POST":
        f = WorkshopProgramSelectForm(request.POST, instance=wpc)
        if f.is_valid():
            wpc = f.save(commit=False)
            wpc.user = u
            wpc.save()
            return redirect("profile")
    else:
        f = WorkshopProgramSelectForm(instance=wpc)

    try:
        wshops = get_workshops_info(u.userprofile.is_adult)
    except ValueError:
        messages.error(request, u"Ошибка #75. Пожалуйста, свяжитесь с администратором")
        return redirect("profile")

    wshops = [ws for ws in wshops if ws["has_intaking_programs"]]
    if len(wshops) == 0:
        messages.error(request, "Набор закрыт. Мастерских, ведущих набор нет.")
        return redirect("profile")
    # program filtering is done in template. Not a fan of that idea, though

    return {"form": f, "workshops": wshops}


@login_required
@user_has_no_buck_with_slug("complete_first_step")
@render_to("intake/provide_contacts.html")
def provide_contacts(request):
    uac, _ = UserAdditionalContacts.objects.get_or_create(user=get_user(request))
    if request.method == "POST":
        f = AdditionalContactsForm(request.POST, instance=uac)
        if f.is_valid():
            f.save()
            return redirect("profile")
    else:
        f = AdditionalContactsForm(instance=uac)

    return {"form": f}


@login_required
@user_has_no_buck_with_slug("complete_first_step")
@render_to("intake/provide_basic_info.html")
def provide_basic_info(request):
    user = get_user(request)

    if request.method == "POST":
        f = BasicInfoForm(request.POST)
        if f.is_valid():
            profile = user.userprofile
            profile.is_male = bool(f.cleaned_data.get('is_male'))
            for target, field in [
                (user, 'first_name'),
                (user, 'last_name'),
                (user, 'email'),
                (profile, 'middle_name'),
                (profile, 'location'),
                (profile, 'birthdate'),
            ]:
                setattr(target, field, f.cleaned_data.get(field))
            profile.save()
            user.save()
            return redirect("profile")
    else:
        f = BasicInfoForm.from_user(user)

    return {"form": f}


@login_required
@user_has_no_buck_with_slug("complete_first_step")
@render_to("intake/collect_education_info.html")
def collect_education_info(request):
    uei = get_object_or_None(UserEducationInfo, user=get_user(request))
    if request.method == "POST":
        f = EducationInfoForm(request.POST, instance=uei)
        if f.is_valid():
            uei = f.save(commit=False)
            uei.user = get_user(request)
            uei.save()
            return redirect("profile")
    else:
        f = EducationInfoForm(instance=uei)

    return {"form": f}


@login_required
@user_has_no_buck_with_slug("complete_first_step")
@render_to("intake/get_activity_info.html")
def get_activity_info(request):
    activity = get_object_or_None(UserActivityInfo, user=get_user(request))
    if request.method == "POST":
        f = ActivityInfoForm(request.POST, instance=activity)
        if f.is_valid():
            a = f.save(commit=False)
            a.user = get_user(request)
            a.save()
            return redirect("profile")
    else:
        f = ActivityInfoForm(instance=activity)
    return {"form": f}


@login_required
@user_has_no_buck_with_slug("complete_first_step")
@render_to("intake/demand_handymanship.html")
def demand_handymanship(request):
    hnd = get_object_or_None(UserHandymanshipInfo, user=get_user(request))
    if request.method == "POST":
        f = HandymanshipInfoForm(request.POST, instance=hnd)
        if f.is_valid():
            a = f.save(commit=False)
            a.user = get_user(request)
            a.save()
            return redirect("profile")
    else:
        f = HandymanshipInfoForm(instance=hnd)
    return {"form": f}


@login_required
@user_has_no_buck_with_slug("complete_first_step")
@render_to("intake/fill_in_money.html")
def fill_in_money(request):
    gibe = get_object_or_None(GiveBackInfo, user=get_user(request))
    if request.method == "POST":
        f = MoneyInfoForm(request.POST, instance=gibe)
        if f.is_valid():
            a = f.save(commit=False)
            a.user = get_user(request)
            a.save()
            return redirect("profile")
    else:
        f = MoneyInfoForm(instance=gibe)
    return {"form": f}


@login_required
@user_has_no_buck_with_slug("complete_first_step")
@render_to("intake/promise_help.html")
def promise_help(request):
    gibe = get_object_or_None(GiveBackInfo, user=get_user(request))
    if request.method == "POST":
        f = GiveBackInfoForm(request.POST, instance=gibe)
        if f.is_valid():
            a = f.save(commit=False)

            skills_dict = {
                "skills": f.cleaned_data.get("skills"),
                "skills_other": f.cleaned_data.get("skills_other"),
            }
            a.relevant_skills = skills_dict
            a.user = get_user(request)
            a.save()
            return redirect("profile")
    else:
        if gibe and gibe.relevant_skills:
            f = GiveBackInfoForm(
                instance=gibe,
                initial={
                    "skills": gibe.relevant_skills["skills"],
                    "skills_other": gibe.relevant_skills["skills_other"],
                },
            )
        else:
            f = GiveBackInfoForm()
    return {"form": f}


@login_required
@user_has_no_buck_with_slug("complete_first_step")
@render_to("intake/choose_my_way.html")
def choose_my_way(request):
    trip = get_object_or_None(UserLogisticsInfo, user=get_user(request))
    if request.method == "POST":
        f = LogisticsInfoForm(request.POST, instance=trip)
        if f.is_valid():
            t = f.save(commit=False)
            t.user = get_user(request)
            t.save()
            return redirect("profile")
    else:
        f = LogisticsInfoForm(instance=trip)
    return {"form": f}


@login_required
@user_has_no_buck_with_slug("complete_first_step")
@render_to("intake/retrospect.html")
def retrospect(request):
    pfpi = get_object_or_None(PastFuturePresentInfo, user=get_user(request))
    if request.method == "POST":
        f = PastFuturePresentInfoForm(request.POST, instance=pfpi)
        if f.is_valid():
            t = f.save(commit=False)
            t.user = get_user(request)
            t.save()
            return redirect("profile")
    else:
        d = {}
        d["form"] = PastFuturePresentInfoForm(instance=pfpi)
        if pfpi is not None and not isinstance(pfpi.been_before, str):
            d["schools_data"] = pfpi.been_before
        return d


@login_required
@user_has_no_buck_with_slug("complete_first_step")
@render_to("intake/leakage.html")
def leakage(request):
    if request.method == "POST":
        f = LeakageInfoForm(request.POST)
        if f.is_valid():
            leak_info, created = PastFuturePresentInfo.objects.get_or_create(user=get_user(request))
            src_dict = {
                "src_select": f.cleaned_data.get("src_select"),
                "src_other": f.cleaned_data.get("src_other"),
            }
            leak_info.leakage_source = src_dict
            leak_info.save()

            return redirect("profile")
    else:
        pfpi = get_object_or_None(PastFuturePresentInfo, user=get_user(request))
        if pfpi and pfpi.leakage_source:
            f = LeakageInfoForm(
                initial={
                    "src_select": pfpi.leakage_source["src_select"],
                    "src_other": pfpi.leakage_source["src_other"],
                },
            )
        else:
            f = LeakageInfoForm()

    return {"form": f}


@login_required
@user_has_no_buck_with_slug("complete_first_step")
@render_to("intake/upload_photo.html")
def upload_photo(request):
    current_photo = get_object_or_None(UserPhoto, user=get_user(request))
    if request.method == "POST":
        f = PhotoUploadForm(request.POST, request.FILES)
        if f.is_valid():
            ph = f.cleaned_data.get("photo")
            if ph:
                uf, _ = UserPhoto.objects.get_or_create(user=get_user(request))
                uf.photo = ph
                uf.save()
                messages.success(request, u"Фотка обновлена")
                return redirect("profile")
    else:
        f = PhotoUploadForm()

    return {"form": f, "current_photo": current_photo}


@login_required
@render_to("intake/parents.html")
def parents(request):
    uu = get_object_or_None(UnderageUserInfo, user=get_user(request))
    if request.method == "POST":
        f = UnderageUserInfoForm(request.POST, instance=uu)
        if f.is_valid():
            uu_info = f.save(commit=False)
            uu_info.user = get_user(request)
            uu_info.save()

            return redirect("profile")
    else:
        f = UnderageUserInfoForm(instance=uu)

    return {"form": f}


@login_required
@user_has_no_buck_with_slug("complete_first_step")
def complete_first_step(request):
    current_user = get_user(request)
    if get_config("INTAKE_IS_CLOSED", False):
        if not has_buck(current_user, "locked_to_workshop"):
            messages.error(request, u"Набор закрыт.")
            return redirect("profile")

    # TODO: заменить на похожую штуку из intake.tools
    if attach_buck(
        current_user,
        "complete_first_step",
        intention=u"Пользователь заполнил профиль на сайте",
    ):
        messages.success(request, u"Вы успешно заполнили анкету")
    else:
        messages.error(request, u"Вы уже выполняли этот шаг!")

    if has_buck(current_user, "locked_to_workshop"):
        app = get_object_or_404(ApplicationCase, user=current_user)
        app.accept()
        attach_buck(app.user, "passed_first_step", intention=u"[auto-pass]")
        app.mark_passed()
        app.save()

        messages.success(request, u"Ваша заявка передана в мастерскую.")

        if prepare_second_round(app):
            subject = u"ЛШ2020: вступительное задание"
            template = loader.get_template("intake/staff/emails/second_round.txt")
            text = template.render(Context({}))
            send_mail_to_applicant(app, subject, text)

            robot_comment_on_case(
                case=app,
                comment=u"У выбранной программы есть автоматически рассылаемое вступительно задание. Участнику отправлено уведомление.",
            )
            messages.success(request, u"Уже можно заполнить вступительно задание")

    return redirect("profile")


@login_required
@render_to("intake/timeline/timeline.html")
def timeline(request):
    u = get_user(request)

    if not has_buck(u, "complete_first_step"):
        return redirect("profile")

    waiting_greetings = [
        'Запасаемся попкорном и ждём',
        'Давайте уже после майских',
        'Кто ждёт &mdash; дождётся',
        'Всё идёт по плану (надеемся)',
        'Ваша анкета пристально изучается',
        'Ещё чуть-чуть и уже лето',
    ]

    context = {
        'jumbo_title': random.choice(waiting_greetings)
    }

    prof = u.userprofile
    photo = get_object_or_None(UserPhoto, user=u)
    context["profile"] = prof
    context["photo"] = photo
    if photo and photo.needs_to_be_changed:
        photo_form = PhotoReplaceForm()
        photo_form.fields["photo"].label = u"Загрузить новое фото"
        context["photo_change_form"] = photo_form

    case = get_object_or_None(ApplicationCase, user=u)
    context["application_case"] = case

    wpc = u.workshoppetitionconnection
    hw = ApplicationAssignmentResponse.all_for_user(
        u, wpc.workshop_slug, wpc.program_slug
    )
    if len(hw) > 1:
        messages.error(
            request,
            u"Проблема с вступительным заданием. Свяжитесь с нами? sarah.connor@letnyayashkola.org",
        )
        context["assignment"] = None
    else:
        context["assignment"] = hw[0] if len(hw) == 1 else None

    if has_buck(u, "nabor_rejected"):
        email = get_object_or_None(
            AppEmail, to_user=u, relevant_buck__slug="nabor_rejected"
        )
        if email is not None:
            context["has_decline_email"] = True
            context["email_subject"] = email.subject
            context["email_text"] = email.text
        else:
            context["has_decline_email"] = False

    if has_buck(u, "declined_by_workshop"):
        email = get_object_or_None(
            AppEmail, to_user=u, relevant_buck__slug="declined_by_workshop"
        )
        if email is not None:
            context["has_decline_email"] = True
            context["email_subject"] = email.subject
            context["email_text"] = email.text
        else:
            context["has_decline_email"] = False

    return context


class OwnAppReadOnly(generic.View):
    template_name = "intake/own_app_readonly.html"

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        user = get_user(request)
        case = get_object_or_None(ApplicationCase, user=user)
        if case is None:
            messages.error(
                request, u"Что-то пошло не так с вашей анкетой. Вы её заполняли? Точно?"
            )
            return redirect("profile")
        return render(request, self.template_name, {"object": case})


class AssignmentDownload(generic.View):
    def get_filename(self, w_slug, p_slug):
        return u"zadanie_2020_%s_%s" % (w_slug, p_slug)

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        u = get_user(request)

        if not has_buck(u, "complete_first_step"):
            return redirect("profile")

        wpc = u.workshoppetitionconnection
        hw = ApplicationAssignmentResponse.all_for_user(
            u, wpc.workshop_slug, wpc.program_slug
        )
        ass = None
        if len(hw) > 1:
            ass = None
        else:
            ass = hw[0] if len(hw) == 1 else None

        if ass is None:
            messages.error(
                request,
                u"Проблема с вступительным заданием. Свяжитесь с нами? sarah.connor@letnyayashkola.org",
            )
            return redirect("/profile/")

        ass = ass.assignment.assignment
        filepath = ass.path
        wrapper = FileWrapper(open(filepath, 'rb'))
        response = HttpResponse(wrapper, content_type="text/plain")
        fname = self.get_filename(wpc.workshop_slug, wpc.program_slug)
        ext = os.path.splitext(filepath)[1]
        response["Content-Disposition"] = u"attachment; filename=%s%s" % (fname, ext)
        response["Content-Length"] = ass.size
        if ext == ".docx":
            mimetype = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        elif ext == ".zip":
            mimetype = "application/zip"
        elif ext == ".pdf":
            mimetype = "application/pdf"
        elif ext == ".xls":
            mimetype = "application/vnd.ms-excel"
        elif ext == ".xlsx": 
            mimetype = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        else:
            mimetype = "application/msword"

        response["mimetype"] = mimetype
        return response


class AssignmentMyResponseDownload(generic.View):
    def get_filename(self, w_slug, p_slug):
        return u"zadanie_moi_otvet_2020_%s_%s" % (w_slug, p_slug)

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        u = get_user(request)

        if not has_buck(u, "complete_first_step"):
            return redirect("profile")

        wpc = u.workshoppetitionconnection
        hw = ApplicationAssignmentResponse.all_for_user(
            u, wpc.workshop_slug, wpc.program_slug
        )
        ass = None
        if len(hw) > 1:
            ass = None
        else:
            ass = hw[0] if len(hw) == 1 else None

        if ass is None:
            messages.error(
                request,
                u"Проблема с вашим ответом на вступительное задание. Свяжитесь с нами? sarah.connor@letnyayashkola.org",
            )
            return redirect("/profile/")

        ass = ass.response
        filepath = ass.path
        wrapper = FileWrapper(open(filepath, 'rb'))
        response = HttpResponse(wrapper, content_type="text/plain")
        fname = self.get_filename(wpc.workshop_slug, wpc.program_slug)
        ext = os.path.splitext(filepath)[1]
        response["Content-Disposition"] = u"attachment; filename=%s%s" % (fname, ext)
        response["Content-Length"] = ass.size
        if ext == ".docx":
            mimetype = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        elif ext == ".zip":
            mimetype = "application/zip"
        elif ext == ".pdf":
            mimetype = "application/pdf"
        elif ext == ".xls":
            mimetype = "application/vnd.ms-excel"
        elif ext == ".xlsx": 
            mimetype = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        else:
            mimetype = "application/msword"

        response["mimetype"] = mimetype
        return response


class AssignmentResponse(generic.View):
    form_class = AssignmentResponseForm
    template_name = "intake/assignments/fill_in.html"
    ext_whitelist = (".doc", ".docx", ".pdf", ".zip", ".xlsx", ".xls")

    def do_render(self, request, form, hw_obj):
        u = get_user(request)
        return render(
            request,
            self.template_name,
            {
                "form": form,
                "homework": hw_obj,
                "wpc": u.workshoppetitionconnection,
                "back_urls": [(reverse("intake:timeline"), u"Назад")],
            },
        )

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        u = get_user(request)
        wpc = u.workshoppetitionconnection
        hw = ApplicationAssignmentResponse.all_for_user(
            u, wpc.workshop_slug, wpc.program_slug
        )
        if len(hw) > 1:
            messages.error(
                request,
                u"O_o. Не могу достать ваше задание. Свяжитесь с нами? sarah.connor@letnyayashkola.org",
            )
            return redirect("profile")
        else:
            hw = hw[0]
        if hw.is_complete:
            return redirect("profile")

        return self.do_render(request, self.form_class(instance=hw), hw)

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        u = get_user(request)
        wpc = u.workshoppetitionconnection
        hw = ApplicationAssignmentResponse.all_for_user(
            u, wpc.workshop_slug, wpc.program_slug
        )
        if len(hw) > 1:
            messages.error(
                request,
                u"O_o. Не могу достать ваше задание. Свяжитесь с нами? sarah.connor@letnyayashkola.org",
            )
            return redirect("profile")
        else:
            hw = hw[0]
        if hw.is_complete:
            return redirect("profile")

        form = self.form_class(request.POST, request.FILES, instance=hw)
        if form.is_valid():
            data = form.cleaned_data["response"]
            filename = data.name
            ext = os.path.splitext(filename)[1]
            ext = ext.lower()
            if ext not in self.ext_whitelist:
                messages.error(
                    request,
                    f"Запрещённый тип документа! Только {', '.join(self.ext_whitelist)}. Никаких {ext}"
                )
            else:
                hw = form.save()
                hw.is_complete = True
                hw.save()
                messages.success(request, u"Задание загружено!")
                return redirect("profile")

        return self.do_render(request, form, hw)


class ReadDeclineEmail(generic.View):
    form_class = AssignmentResponseForm
    template_name = "intake/email_render.html"

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        u = get_user(request)

        email = get_object_or_404(
            AppEmail, to_user=u, relevant_buck__slug="declined_by_workshop"
        )

        return render(
            request,
            self.template_name,
            {
                "email_subject": email.subject,
                "email_text": email.text,
                "back_url": reverse("profile"),
            },
        )


class ReplacePhoto(generic.View):
    form_class = PhotoReplaceForm

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        # FIXME: тут надо подумать, нет ли опасности залить чужое фото.
        u = get_user(request)
        photo = get_object_or_404(UserPhoto, user=u)
        if photo.needs_to_be_changed is False:
            messages.error(request, u"Не надо так.")
            return redirect("profile")

        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            photo.photo = form.cleaned_data.get("photo")
            photo.needs_to_be_changed = False
            photo.save()
            messages.success(request, u"Фотка обновлена, спасибо.")

            app = get_object_or_None(ApplicationCase, user=u)
            if app is not None:
                robot_comment_on_case(case=app, comment=u"Участник заменил фото")
        else:
            messages.error(
                request,
                u"Не могу сохранить фото. Вы не забыли приложить файл? Попробуйте закачать фото ещё раз",
            )

        return redirect("profile")


@login_required
@ajax_request
def emails(request):
    emails = AppEmail.objects.filter(to_user=get_user(request)).values_list(
        "subject", "text", "relevant_buck__slug", "date_created"
    )
    return [
        {
            "subject": subject,
            "text": html.linebreaks(text),
            "buck": buck,
            "date": u"%s" % date,
        }
        for subject, text, buck, date in emails
    ]


class Messages(generic.View):
    template_name = "intake/messages.html"

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


@ajax_request
def workshop_programs(request):
    return [
        {
            "workshop_slug": w_slug,
            "program_slug": p_slug,
            "workshop_name": w_name,
            "program_name": p_name,
        }
        for w_slug, w_name, p_slug, p_name in get_wp_slugs_and_names()
    ]


class SecretSignUp(generic.View):
    template_name = "intake/secret-signup.html"
    form_class = FancySignupForm

    def get_locked_wp_tuple(self):
        """
        define something that returns wp tuple
        like ('organizer', 'rhetoric') or similar
        """
        raise NotImplementedError

    def get_locked_wp_html_header(self):
        """
        define something that returns string for this thing
        """
        raise NotImplementedError

    def get_locked_wp_html_middle_meat(self):
        """ define str for this explanatory part """
        raise NotImplementedError

    def get_locked_wp_html_tail(self):
        """ define str for this tail thing part """
        return u"Зарегистрироваться на хитрый модуль"

    def do_render(self, request, form):
        return render(request, self.template_name, {"form": form})

    def get_header(self):
        from crispy_forms.layout import Layout, HTML, Div

        locked_w_slug, locked_p_slug = self.get_locked_wp_tuple()
        header = self.get_locked_wp_html_header()
        middle_meat = self.get_locked_wp_html_middle_meat()
        return Layout(
            Div(
                HTML(u"<h1 class='panel-title'>%s</h1>" % header),
                css_class="panel-heading text-center",
            ),
            HTML(
                u"""<p class='text-center'>Да. Это тут. <span class='text-muted'>Уже регистрировались? <a href='/accounts/login/'>Войдите</a></span></p>
<p class='text-center'>%s<br/><b>Без мук выбора.</b></p>
<p class='text-center'>Заполняете остальную часть анкеты, отправляете,
попадаете на рассмотрение кураторам.</p>"""
                % middle_meat
            ),
        )

    def get_tail(self):
        from crispy_forms.layout import Layout, Submit
        from crispy_forms.bootstrap import FormActions

        tail = self.get_locked_wp_html_tail()
        return Layout(
            FormActions(Submit("submit", tail, css_class="btn btn-primary btn-block"))
        )

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        from crispy_forms.layout import Layout, Div
        from crispy_forms.helper import FormHelper

        helper = FormHelper()
        helper.layout = Layout(
            self.get_header(),
            Div(default_signup_fields, self.get_tail(), css_class="panel-body signup"),
        )
        helper.help_text_inline = True
        helper.form_style = "inline"
        form.helper = helper

        return self.do_render(request, form)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        locked_w_slug, locked_p_slug = self.get_locked_wp_tuple()
        if form.is_valid():
            new_u = form.save(request)
            create_locked_wpc(
                new_u,
                w_slug=locked_w_slug,
                p_slug=locked_p_slug,
                motivation=u"[вот тут надо что-то вменяемое написать]",
            )
            # FIXME: убрать эту логику вообще

        return self.do_render(request, form)


class SecretRozetkaSignUp(SecretSignUp):
    def get_locked_wp_tuple(self):
        return ("obrazovanie", "rozetka")

    def get_locked_wp_html_header(self):
        return (
            u"Это тут у вас можно зарегистрироваться на модуль &laquo;Розетка&raquo;?"
        )

    def get_locked_wp_html_middle_meat(self):
        return u"Порядок действий простой. Вы заполняете анкету, и привязываетесь к именно вот мастерской &laquo;Образования&raquo;, именно к модулю &laquo;Розетка&raquo;."


class SecretNeudobnoSignUp(SecretSignUp):
    def get_locked_wp_tuple(self):
        return ("obrazovanie", "neudobno")

    def get_locked_wp_html_header(self):
        return u"Это тут у вас можно зарегистрироваться на модуль &laquo;Неудобный разговор&raquo;?"

    def get_locked_wp_html_middle_meat(self):
        return u"Порядок действий простой. Вы заполняете анкету, и привязываетесь к именно вот мастерской &laquo;Образования&raquo;, именно к модулю &laquo;Неудобный разговор&raquo;."


class SecretInclusioSignUp(SecretSignUp):
    def get_locked_wp_tuple(self):
        return ("obrazovanie", "inclusio")

    def get_locked_wp_html_header(self):
        return (
            u"Это тут у вас можно зарегистрироваться на модуль &laquo;Инклюзия&raquo;?"
        )

    def get_locked_wp_html_middle_meat(self):
        return u"Порядок действий простой. Вы заполняете анкету, и привязываетесь к именно вот мастерской &laquo;Образования&raquo;, именно к модулю &laquo;Инклюзия&raquo;."


class SecretObrZhurSignUp(SecretSignUp):
    def get_locked_wp_tuple(self):
        return ("obrazovanie", "zhurnalistika")

    def get_locked_wp_html_header(self):
        return u"Это тут у вас можно зарегистрироваться на модуль &laquo;Образовательная журналистика&raquo;?"

    def get_locked_wp_html_middle_meat(self):
        return u"Порядок действий простой. Вы заполняете анкету, и привязываетесь к именно вот мастерской &laquo;Образования&raquo;, именно к модулю &laquo;Образовательная журналистика&raquo;."


class SecretHardOnGeo(SecretSignUp):
    def get_locked_wp_tuple(self):
        return ("scientificskills", "geo")

    def get_locked_wp_html_header(self):
        return u"Это тут у вас можно зарегистрироваться на гео-трек?"

    def get_locked_wp_html_middle_meat(self):
        return u"Порядок действий простой. Вы заполняете анкету, и привязываетесь к именно вот мастерской &laquo;Hard&Soft&nbsp;scientific&nbsp;skills&raquo;, именно к геотреку."


class Secret105SignUp(SecretSignUp):
    def get_locked_wp_tuple(self):
        return ("105element", "main")

    def get_locked_wp_html_header(self):
        return u"Это тут у вас можно зарегистрироваться на супер крутую мастерскую физики &laquo;105 элемент&raquo;?"

    def get_locked_wp_html_middle_meat(self):
        return u"Порядок действий простой. Вы заполняете анкету, и привязываетесь к именно вот мастерской &laquo;105 элемент&raquo;."


class SecretSciPubNauSignUp(SecretSignUp):
    def get_locked_wp_tuple(self):
        return ("sci-pub", "nauchkom")

    def get_locked_wp_html_header(self):
        return u"Это тут у вас регистрируют на &laquo;Научные коммунакции&raquo;?"

    def get_locked_wp_html_middle_meat(self):
        return u"Порядок действий простой. Вы заполняете анкету, и привязываетесь к именно вот мастерской &laquo;ШНЖ&raquo;, именно к направлению научных коммуникаций."


class SecretLawSignUp(SecretSignUp):
    def get_locked_wp_tuple(self):
        return ("itip-law", "main")

    def get_locked_wp_html_header(self):
        return u"Это тут у вас регистрируют на &laquo;IT&IP law jam&raquo;?"

    def get_locked_wp_html_middle_meat(self):
        return u"Порядок действий простой. Вы заполняете анкету, и привязываетесь к именно вот мастерской &laquo;IT&IP law jam&raquo; с целью познать право для IT и творчества."


class SecretFreestyleSignUp(SecretSignUp):
    def get_locked_wp_tuple(self):
        return ("zhivoyteatr", "freestyle")

    def get_locked_wp_html_header(self):
        return u"Это тут у вас регистрируют на &laquo;Фристайл&raquo;?"

    def get_locked_wp_html_middle_meat(self):
        return u"Порядок действий простой. Вы заполняете анкету, и привязываетесь к именно вот мастерской &laquo;Живой театр&raquo; на направление freestyle."


class SecretFRCSignUp(SecretSignUp):
    def get_locked_wp_tuple(self):
        return ("frc", "main")

    def get_locked_wp_html_header(self):
        return u"Это вот это секретная ссылка как попасть на ЦПИ?"

    def get_locked_wp_html_middle_meat(self):
        return u"Порядок действий простой. Вы заполняете анкету и сразу же прикрепляетесь к ЦПИ и без вариантов и прочих мук выбора. Как заполните анкету, её увидят соответствующие кураторы."


class SecretGenericSignUp(SecretSignUp):
    def get_locked_wp_tuple(self):
        return ("generic", "main")

    def get_locked_wp_html_header(self):
        return u"Это вот это секретная ссылка как пройти набор когда он закрыт?"

    def get_locked_wp_html_middle_meat(self):
        return u"Порядок действий простой. Вы заполняете анкету, и пишете человеку с которым вы договорились чтобы вас привязали к нужному направлению. И только после этого вас увидят среди заявок выбранного направления/мастерской."


class WPCMotivationEdit(generic.View):
    form_class = WPCMotivationForm

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        u = get_user(request)
        wpc = get_object_or_None(WorkshopPetitionConnection, user=u)
        form = self.form_class(request.POST, instance=wpc)
        if form.is_valid():
            # Get the motivation and update only it
            cd = form.cleaned_data
            wpc.wait_but_why = cd["wait_but_why"]
            wpc.save()
        else:
            messages.error(request, u"Ошибка при заполнении мотивации. Try again")
        return redirect("profile")
