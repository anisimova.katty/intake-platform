from annoying.functions import get_config


class MyAccountAdapter:
    def is_open_for_signup(self, request):
        return not get_config("INTAKE_IS_CLOSED", False)
