import os
import environ
from curators_dict_2019 import curators_dict

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

env = environ.Env(DEBUG=(bool, False), ALLOWED_HOSTS=(str,))

DEBUG = env("DEBUG")

ALLOWED_HOSTS = ["*"] if DEBUG else env("ALLOWED_HOSTS")

LOGIN_REDIRECT_URL = "/accounts/profile/"

ROBOT_USERNAME = "sarah.connor"
INTAKE_IS_CLOSED = True

CURATORS = curators_dict

DATABASES = {"default": env.db()}

SECRET_KEY = env("SECRET_KEY")

MEDIA_ROOT = os.path.join(BASE_DIR, "media")
MEDIA_URL = "/media/"

STATIC_ROOT = os.path.join(BASE_DIR, "static")
STATIC_URL = "/static/"

# This email is used when we send an email using regular django stuff
DEFAULT_FROM_EMAIL = "no-reply@letnyayashkola.ru"

ADMINS = (("garik", "garik@letnyayashkola.org"),)

MANAGERS = ADMINS
SERVER_EMAIL = "no-reply-no-plz@letnyayashkola.ru"

EMAIL_HOST = "localhost"
EMAIL_PORT = 25
EMAIL_USE_TLS = False

EMAIL_HOST_USER = u""
EMAIL_HOST_PASSWORD = r""

LSH_DATES_DICT = {"start": (2019, 7, 6), "end": (2019, 8, 5)}
TELEGRAM_BOT_TOKEN = "376035212:AAFxlYtiLIa1vL09Nkz0p9XDzQfmyQj-u44"
