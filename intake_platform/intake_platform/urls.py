from django.conf.urls import include, url
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from intake_platform.common import views as common_views

from .views import IndexView, TestView, logout

urlpatterns = [
    path("admin/", admin.site.urls),
    url(r"^$", IndexView.as_view(), name="index"),
    url(r'logout', logout),
    url(r"^test/$", TestView.as_view(), name="test"),
    # third-party stuff
    url(r"^selectable/", include("selectable.urls")),
    url(r"^accounts/profile/$", common_views.profile, name="profile"),
    url(r"^help/$", common_views.help, name="help"),
    # intake
    url("", include(("intake_platform.intake.urls", "intake"), namespace="intake")),
    # curators
    url(
        "",
        include(
            ("intake_platform.intake.curators.urls", "curators"), namespace="curators"
        ),
    ),
    # guru
    url("", include(("intake_platform.intake.guru.urls", "guru"), namespace="guru")),
    url('', include('django.contrib.auth.urls')),
    url('', include('social_django.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
